###
# download and extract
#
# we don't need maven, but this image is available for building the other dariah services and has curl 
# and jar for download and extract ;-)
###
FROM maven:3.8.3-jdk-8 as builder

ARG JOLOKIA_VERSION="1.3.7" # jolokia as of 1.5.0 requires auth for proxy operation
ARG FITS_SERVLET_VERSION="1.1.3"
ARG FITS_VERSION="1.2.0"

# download and explode jolokia
RUN curl -XGET "https://repo1.maven.org/maven2/org/jolokia/jolokia-war/${JOLOKIA_VERSION}/jolokia-war-${JOLOKIA_VERSION}.war" --output /jolokia.war
RUN mkdir /jolokia && cd /jolokia && jar -xf ../jolokia.war

# download and explode fits
RUN curl -XGET "https://projects.iq.harvard.edu/files/fits/files/fits-${FITS_SERVLET_VERSION}.war" --output /fits.war
RUN mkdir /fits && cd /fits && jar -xf ../fits.war

RUN curl -XGET "https://projects.iq.harvard.edu/files/fits/files/fits-${FITS_VERSION}.zip" --output /fits.zip
RUN unzip /fits.zip -d /
RUN mv /fits-${FITS_VERSION} /app

###
# assemble image
###
FROM tomcat:8.5-jre8

ENV JAVA_OPTS="-Xmx768m"
ENV CATALINA_OPTS="-Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

COPY Dockerfile /

COPY --from=builder /jolokia /usr/local/tomcat/webapps/jolokia
COPY --from=builder /fits /usr/local/tomcat/webapps/fits
COPY --from=builder /app /app
COPY ./log4j.properties /app/log4j.properties

RUN echo "fits.home=/app" >> /usr/local/tomcat/conf/catalina.properties \
    && echo "shared.loader=/app/lib/*.jar" >> /usr/local/tomcat/conf/catalina.properties \
    && echo "log4j.configuration=/app/log4j.properties" >> /usr/local/tomcat/conf/catalina.properties

RUN addgroup --system --gid 1012 tomcat-fits \
    && adduser --system --uid 1012 --gid 1012 tomcat-fits \
    && chown -R tomcat-fits:tomcat-fits /usr/local/tomcat/webapps/ \
    && chown -R tomcat-fits:tomcat-fits /app

# add tomcat-fits to group ULSB, so it can read cruds tomcat temp dir
RUN addgroup --system --gid 29900 ULSB \
    && usermod -a -G ULSB tomcat-fits

USER tomcat-fits
WORKDIR /usr/local/tomcat/

